//my $data = {
//  [x] id => 101,
//  [x] name => 'Example',
//  [ ] asignee => 'Junior',
//  [x] color => '#334455',
//  [x] start => '2018-01-01',
//  [x] end => undef,
//  [x] days => 30, # days
//  [x] maxdays => 50, # days
//  [x] buffer => 'ALL', # WEEK
//  [-] blocks => [],
//  [x] depends_on => {
//  [?]     blocking_type => 'AND|OR',
//  [x]     id => 123
//  [ ]     daysbefore => 10
//  [ ]     daysafter => 15
//  [ ]     id => []
//      },
//  [x] event_group => {
//  [x]     name => 'general',
//  [ ]     color => 'black'
//      },
//  [?] tags => ['important','phone']
//
//};

var colores = {
    'ambos': "#f0f0f0",
    'cata': '#ffc0cb', // 'pink',
    'yo': '#87cefa', // 'lightskyblue'
    'champions': '#483d8b', // 'darkslateblue',
};

var ganttDataEurope = [
    {
        id: 2, name: "Viajes", series: [
            { id: 201, name: "Richard", start: new Date(2018,02,23), end: new Date(2018,03,07), color: colores["cata"] },
            { id: 202, name: "EZE-BAR", start: new Date(2018,03,08), end: new Date(2018,03,08), color: colores["yo"] },
            { id: 203, name: "BAR-MAD", start: new Date(2018,03,09), end: new Date(2018,03,09), color: colores["yo"] },
            { id: 204, name: "Pais Vasco", start: new Date(2018,03,28), end: new Date(2018,04,03), color: colores["ambos"] },
            { id: 205, name: "Finde largo", start: new Date(2018,04,11), end: new Date(2018,04,14), color: colores["cata"] },
            { id: 211, name: "Excursion", start: new Date(2018,04,15), end: new Date(2018,04,19), color: colores["cata"] },
            { id: 206, name: "Finde largo", start: new Date(2018,04,25), end: new Date(2018,04,27), color: colores["yo"] },
            { id: 207, name: "MAD-FEZ", start: new Date(2018,04,15), end: new Date(2018,04,15), color: colores["yo"] },
            { id: 208, name: "Receso", start: new Date(2018,05,02), end: new Date(2018,05,18), color: colores["cata"] },
            { id: 209, name: "Belgica-Holanda", start: new Date(2018,05,02), end: new Date(2018,05,10), color: colores["ambos"] },
            { id: 210, name: "Limite 90 dias", start: new Date(2018,06,07), end: new Date(2018,06,07), color: colores["yo"] }
        ]
    },
    {
        id: 1, name: "Ciudadania", series: [
            { id: 101, name: "Reservar Cita", start: new Date(2018,01,05), end: new Date(2018,01,05), color: colores['cata'] },
            { id: 102, name: "Envio de papeles", start: new Date(2018,01,23), end: new Date(2018,02,05), color: colores['cata'] },
            { id: 103, name: "Cita", start: new Date(2018,02,06), end: new Date(2018,02,06), color: colores['cata'] },
            // entre 30 y 50
            { id: 104, name: "Tramite", depends: 103, days: 60, maxdays: 180, color: colores['cata'] },
            { id: 105, name: "Retirar doc.", depends: 104, days: 1, color: colores['cata'] }
        ]
    },
    {
        id: 3, name: "Matrimonio", series: [
            // 30 dias antes
            { id: 301, name: "Reservar Cita", start: new Date(2018,01,27), end: new Date(2018,01,27), color: colores["cata"] },
            // 30 dias antes
            { id: 302, name: "Espera Cita", start: new Date(2018,01,28), end: new Date(2018,03,11), color: colores["ambos"] },
            // Presentacion de la documentacion / carpeta
            // { name: "Presentacion de documentacion", start: new Date(2018,03,12), end: new Date(2018,03,12), color: colores["cata"] },
            // Presentacion de carpeta (tenemos que estar los dos + 1 testigo)
            { id: 303, name: "Presentar carpeta", start: new Date(2018,03,12), days: 1, color: colores['ambos'] },
            // entre 30 y 60 dias
            { id: 304, name: "Tramite", depends: 303, days: 30, maxdays: 60, color: colores['ambos'] },
            // un dia
            { id: 305, name: "Reservar cita", depends: 303, days: 1, color: colores['ambos'] },
            // 15-20 dias de espera aprox
            // { name: "Espera cita", start: new Date(2018,04,13), end: new Date(2018,04,28), color: colores['ambos'] },
            // 1
            { id: 306, name: "Cita", depends: 304, days: 1, color: colores['ambos'] }
        ]
    },
    {
        id: 4, name: "Tarjeta Residencia", series: [
            // dias de espera ?
            //{ id: 401, name: "Reservar Cita", start: new Date(2018,04,30), days: 14, color: colores["yo"] },
            { id: 402, name: "Cita", depends: 306, days: 1, color: colores["yo"] },
            // 3 semanas promedio aprox. inmediato???
            { id: 403, name: "Tramite", depends: 402 , days: 14, maxdays: 60, color: colores["yo"] },
            // 1 dia
            { id: 404, name: "Buscar doc.", depends: 403, days: 1, color: colores["yo"] }
        ]
    },
];

var ganttDataEvents = [
    {
        id: 5, name: "Eventos", series: [
            { name: "Champions", start: new Date(2018,03,10), end: new Date(2018,03,11), color: colores["champions"] },
            { name: "Aleti-Levante", start: new Date(2018,03,15), end: new Date(2018,03,15), color: colores["yo"] },
            { name: "Bar&ccedil;a-Valencia", start: new Date(2018,03,15), end: new Date(2018,03,15), color: colores["yo"] },
            { name: "Real-Athletic", start: new Date(2018,03,18), end: new Date(2018,03,18), color: colores["yo"] },
            { name: "Bar&ccedil;a-Sevilla.", start: new Date(2018,03,21), end: new Date(2018,03,21), color: colores["yo"] },
            { name: "Aleti-Betis", start: new Date(2018,03,22), end: new Date(2018,03,22), color: colores["yo"] },
            { name: "Champions", start: new Date(2018,03,24), end: new Date(2018,03,25), color: colores["champions"] },
            { name: "Real-Leganes", start: new Date(2018,03,29), end: new Date(2018,03,29), color: colores["yo"] },
            { name: "Champions", start: new Date(2018,04,01), end: new Date(2018,04,02), color: colores["champions"] },
            { name: "Aleti-Espanyol", start: new Date(2018,04,06), end: new Date(2018,04,06), color: colores["yo"] },
            { name: "Bar&ccedil;a-Real", start: new Date(2018,04,06), end: new Date(2018,04,06), color: colores["yo"] },
            { name: "Bar&ccedil;a-Villareal", start: new Date(2018,04,09), end: new Date(2018,04,09), color: colores["yo"] },
            { name: "Getafe-Aleti", start: new Date(2018,04,13), end: new Date(2018,04,13), color: colores["yo"] },
            { name: "Real-Celta", start: new Date(2018,04,13), end: new Date(2018,04,13), color: colores["yo"] },
            { name: "Aleti-Eibar", start: new Date(2018,04,20), end: new Date(2018,04,20), color: colores["yo"] },
            { name: "Bar&ccedil;a-RealSoc.", start: new Date(2018,04,20), end: new Date(2018,04,20), color: colores["yo"] },
            { name: "Champions", start: new Date(2018,04,26), end: new Date(2018,04,26), color: colores["champions"] },
        ]
    },
    {
        id: 6, name: 'Mundial', series: [
            { name: "Venta Entradas", start: new Date(2018,02,13), end: new Date(2018,03,03), color: colores["yo"] },
            { name: "MUNDIAL", start: new Date(2018,05,14), end: new Date(2018,06,15), color: colores["yo"] },
            { name: "ARG - ISL", start: new Date(2018,05,16), end: new Date(2018,05,16), color: colores["yo"] },
            { name: "ARG - CRO", start: new Date(2018,05,21), end: new Date(2018,05,21), color: colores["yo"] },
            { name: "ARG - NIG", start: new Date(2018,05,26), end: new Date(2018,05,26), color: colores["yo"] },
        ],
    },
];

var ganttDataTest = [
    {
        id: 1, name: "Test Days", series: [
            { id: 1, name: "Start + End", start: new Date(2018,01,01), end: new Date(2018,01,10) },
            { id: 2, name: "Start + Days", start: new Date(2018,01,01), days: 10 },
            { id: 3, name: "Days + End", days: 10, end: new Date(2018,01,10) },
            // { name: "After + End", start: new Date(2018,01,01), end: new Date(2018,01,01) },
            // { name: "After + Days", start: new Date(2018,01,01), end: new Date(2018,01,01) },
            // { name: "Before + Start", start: new Date(2018,01,01), end: new Date(2018,01,01) },
            // { name: "Before + Days", start: new Date(2018,01,01), end: new Date(2018,01,01) },
        ]
    },
    {
        id: 2, name: "Test Dependencies", series: [
            { id: 1, name: "Start + End", start: new Date(2018,01,01), end: new Date(2018,01,10) },
            { id: 2, name: "Depends + End", end: new Date(2018,02,20), depends: 1 },
            { id: 3, name: "Depends + Days", days: 20, depends: 1 },
            { id: 4, name: "Days + End", days: 10, end: new Date(2018,01,10) },
            { id: 5, name: "Depends + End", end: new Date(2018,02,20), depends: 4 },
            { id: 6, name: "Depends + Days", days: 20, depends: 4 },
        ]
    },
    {
        id: 3, name: "Pesimistic", series: [
            { id: 1, name: "Start + End + Buffer", start: new Date(2018,01,01), end: new Date(2018,01,10) },
            { id: 2, name: "Depends + End", end: new Date(2018,02,20), depends: 1 },
            { id: 3, name: "Depends + Days", days: 20, depends: 1 },
            { id: 4, name: "Days + End", days: 10, end: new Date(2018,01,10) },
            { id: 5, name: "Depends + End", end: new Date(2018,02,20), depends: 4 },
            { id: 6, name: "Depends + Days", days: 20, depends: 4 },
        ]
    },
];

var pesimistic = getUrlParam("pesimistic");

if ( pesimistic == '1' ) {
    pesimistic = true;
}else{
    pesimistic = false;
}


processData( ganttDataEurope, pesimistic );
processData( ganttDataTest, pesimistic );
processData( ganttDataEvents, pesimistic );


function getUrlParam(paramName){
    var url_string = window.location.href;
    var url = new URL(url_string);
    return url.searchParams.get(paramName);
}

function addDays(date, days) {
    var myDate = new Date();
    var miliseconds = 24*60*60*1000;
    var milisecondsOffset = miliseconds * days;
    myDate.setTime( date.getTime() + milisecondsOffset );

    return myDate;
}

function processData(data, pesimistic=false){

    for (var j = 0; j < data.length; j++) {
        for (var i = 0; i < data[j].series.length; i++) {
            var task = data[j].series[i];

            processTask( data, task, pesimistic );
        }
    }

    return;
}

function getTaskById(data, task_id){

    for (var j = 0; j < data.length; j++) {
        for (var i = 0; i < data[j].series.length; i++) {
            var task = data[j].series[i];

            if( task.id == task_id ){
                return task;
            }
        }
    }

    return;
}

function processTask( data, task, pesimistic){

    if( 'processed' in task ){
        return task;
    }

    // EVENT START
    if( 'start' in task ){

        // nothing

    }else if( 'depends' in task ){

        var otherTask = getTaskById( data, task.depends );
        processTask( data, otherTask, pesimistic );

        // TODO: check end OR buffer depending view mode [OPTIMISTIC|PESIMISTIC]
        if( pesimistic && 'buffer' in otherTask ){
            task.start = addDays( otherTask.buffer, 1);
        }else{
            task.start = addDays( otherTask.end, 1);
        }

    }else if( 'days' in task && 'end' in task){
        
        task.start = addDays( task.end, -task.days + 1 );

    }

    // EVENT END
    if( 'end' in task ){
        // nothing
    }else if( 'maxdays' in task ){
        
        task.end    = addDays( task.start, task.days - 1);
        task.buffer = addDays( task.start, task.maxdays - 1);

    }else if( 'blocks' in task ){
        
        // nothing

    }else if( 'days' in task && 'start' in task ){
        
        task.end = addDays( task.start, task.days - 1);

    }

    task.processed = true;

    return;
}


